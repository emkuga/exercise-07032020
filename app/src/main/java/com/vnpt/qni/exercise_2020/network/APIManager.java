package com.vnpt.qni.exercise_2020.network;

import com.vnpt.qni.exercise_2020.DataModel.Item;
import com.vnpt.qni.exercise_2020.request.LoginRequest;
import com.vnpt.qni.exercise_2020.response.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIManager {
    String SERVER_URL = "http://bms.mis.vn";
    @POST("/api/MobileNhanVien/DangNhap")
    Call<LoginResponse> Login(@Body LoginRequest loginRequest);

}
